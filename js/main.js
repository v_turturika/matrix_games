function createMatrix() {

    $("#matrix").remove();
    $("#result").remove();
    $("main").hide();
    $("#ab").empty();
    $("#answer").empty();


    var n = $("#n").val(),
        m = $("#m").val();

    var matrix = $("<table></table>").attr({"id": "matrix"}).css({"display": "inline-block"})

    for (var i = 0; i < n; i++) {

        var row = $("<tr></tr>");
        matrix.append(row);

        for (var j = 0; j < m; j++) {

            var item = $("<input/>").attr({type: "text", "class": "r" + i});
            item.wrap("<td></td>");
            row.append(item);
        }
    }

    $("#mtr").append(matrix);
}

function getMatrix() {

    var n = +$("#n").val(),
        m = +$("#m").val();

    var p = [];
    for (var i = 0; i < n; i++) {
        p[i] = [];
        for (var j = 0; j < m; j++) {

            p[i][j] = +$(".r" + i)[j].value;
        }
    }
    return p;
}

function checkCleanStrategies(p) {

    var n = p.length,
        m = p[0].length,
        i, j, I, J;

    var alpha = -Infinity;

    for (i = 0; i < n; i++) {

        var temp = alpha;
        alpha = Math.max(alpha, Math.min.apply(null, p[i]));
        I = (temp != alpha) ? i : I;
    }

    var beta = Infinity;
    for (j = 0; j < m; j++) {

        var max = -Infinity;
        for (i = 0; i < n; i++) {

            max = Math.max(max, p[i][j]);
        }

        temp = beta;
        beta = Math.min(beta, max);
        J = (temp != beta) ? j : J;
    }

    var result = {a: alpha, b: beta, i: ++I, j: ++J};

    result.clear = (alpha == beta);
    return result;

}

function compressMatrix(p) {

    var n = p.length,
        i, j;

    for (var k = 0; k < 2; k++) {
        for (i = 0; i < n; i++) {

            if (p[i] == null) {
                j = i;
                while (j < n - 1) {
                    if (p[j + 1]) p[j] = p[j + 1].slice();
                    else p[j] = p[j + 1];

                    j++;
                }
                n--;
            }
        }
        p.length = n;
    }

    return n;
}

function compareArrays(a, sign, b) {

    for (var i = 0; i < a.length; i++) {

        if (sign == "<=" && a[i] > b[i]) {
            return false;
        }
        else if (sign == ">=" && a[i] < b[i]) {
            return false;
        }
    }
    return true;
}

function checkDominateStrategies(p, mode, arr) {

    var n = p.length,
        i, j;

    arr.length = 0;

    for (i = 0; i < n; i++) {

        for (j = 0; j < n; j++) {

            if (i == j || p[i] == null || p[j] == null) {
                continue;
            }

            if (mode == "columns" && compareArrays(p[i], ">=", p[j])) {

                p[i] = null;
                arr.push(i);
                break;
            }
            else if (mode == "rows" && compareArrays(p[i], "<=", p[j])) {

                p[i] = null;
                arr.push(i);
                break;
            }
        }
    }

    if (n != compressMatrix(p))return true;
    else return false;
}

function crossOutDominateStrategies(p, crossOuted) {

    var temp = p;

    crossOuted.rows = [];
    crossOuted.columns = [];
    for (var i = 0; i < p.length; i++) crossOuted.rows[i] = 0;
    for (i = 0; i < p[0].length; i++) crossOuted.columns[i] = 0;
    var arr = [];

    do {
        crossOuted.rows = setPlace(crossOuted.rows, arr);
        temp = transpose(temp);
        checkDominateStrategies(temp, "columns", arr);
        crossOuted.columns = setPlace(crossOuted.columns, arr);
        temp = transpose(temp);
    }
    while (checkDominateStrategies(temp, "rows", arr));

    return temp;
}

function setPlace(place, obj) {

    for (var i = 0; i < obj.length; i++) {

        var count = 0, pos = 0;
        while ((pos = place.indexOf(-1, pos) != -1 && pos <= obj[i])) {
            pos++;
            count++
        }

        place[obj[i] + count] = -1;
    }

    return place;
}

function transpose(a) {

    var b = [],
        n = a.length,
        m = a[0].length,
        i, j;

    for (j = 0; j < m; j++) {

        b[j] = [];
        for (i = 0; i < n; i++) {

            b[j][i] = a[i][j];
        }
    }

    return b;
}

function calculate() {

    var p = getMatrix();
    $("#ab").empty();
    $("#answer").empty();
    $("input").removeClass('strike');

    var minmax = checkCleanStrategies(p);

    if (minmax.clear) {

        showAlphaBeta(minmax);
        var clear = $("<p></p>").text("Чиста стратегія: (A"+ minmax.i + ", B" + minmax.j + ")"),
            price = $("<p></p>").text("Ціна гри = " + minmax.a);

        $("#answer").append(clear).append(price);
    }
    else {

        showAlphaBeta(minmax)

        if ($("#normalize").prop("checked")) {
            var offset = 0;
            offset = normalize(p);
        }

        var crossOuted = {};
        p = crossOutDominateStrategies(p, crossOuted);

        strikeCells(crossOuted);

        var n = p.length,
            m = p[0].length,
            i, j,
            s = new Simplex(),
            f = [];

        for (i = 0; i < m; i++) f.push(1);

        s.set({
            f: f,
            mode: "max",
            fraction: true
        });
        for (i = 0; i < n; i++) {
            var temp = [];
            for (j = 0; j < m; j++) {

                temp.push(p[i][j]);
            }
            s.set({
                c: {
                    a: temp,
                    b: 1,
                    sign: "<="
                }
            });
        }

        var solution = s.get();
        showTable(s);

        var gamePrice = Fraction(1).div(solution.result),
            Sb = solution.resultX.slice();

        Sb.forEach(function (item, i, arr) {
            arr[i] = Fraction(arr[i]).mul(Fraction(gamePrice));
        });
        Sb = setAnswerStrategy(Sb, crossOuted.columns);

        var Sa = [];
        for (i = s.get().realNumVariables; i < s.get().numVars; i++) {

            Sa.push(s.get(s.get().numSteps - 1).step.delta[i]);
        }

        Sa.forEach(function (item, i, arr) {
            arr[i] = Fraction(arr[i]).mul(Fraction(gamePrice));
        });
        Sa = setAnswerStrategy(Sa, crossOuted.rows);


        if (offset != 0) {
            gamePrice = Fraction(gamePrice).sub(Fraction(offset));
        }

        var answer = $("#answer");
        answer.append("<p></p>").text("Ціна гри = " + Fraction(gamePrice).toFraction(false));

        var strSa = "", strSb = "";

        Sa.forEach(function(item){

            strSa += Fraction(item).toFraction(false) + ", ";
        });

        Sb.forEach(function(item){

            strSb += Fraction(item).toFraction(false) + ", ";
        });

        strSa = strSa.slice(0,-2); strSa = "(" + strSa + ")";
        strSb = strSb.slice(0,-2); strSb = "(" + strSb + ")";

        var outSa = $("<p></p>").text("Sa* = " + strSa),
            outSb = $("<p></p>").text("Sb* = " + strSb);

        answer.append(outSa).append(outSb);
    }

}

function setAnswerStrategy(arr, pattern) {

    var result = pattern.slice(),
        j = 0;
    for (var i = 0; i < arr.length; i++) {

        while (result[j] == -1 && j < pattern.length) result[j++] = 0;
        result[j++] = arr[i];
    }

    while(j <  pattern.length)  result[j++] = 0;
    return result;
}

function showTable(s) {

    $("#result").remove();

    var f = s.get().f,
        n = s.get().numConds,
        m = s.get().numVars;

    var stepsCount = s.get().numSteps;
    var section = "s1";

    var table = $("<table></table>").attr("id", "result");

    $("main").show().append(table);

    tableHeader(table, f);

    for (var k = 0; k < stepsCount; k++) {

        var step = s.get(k).step;
        for (var i = 0; i < n + 1; i++) {

            var row = $("<tr></tr>").addClass(section);
            table.append(row);

            if (i != n) { //row of delta
                row.append($("<td></td>").text("x" + (step.basis[i] + 1)));
                row.append($("<td></td>").text(f[step.basis[i]]));
                row.append($("<td></td>").text(Fraction(step.a[i].b).toFraction(false)));
            }
            else {
                row.append($("<td></td>").text("Delta"));
                row.append($("<td></td>").text(Fraction(step.result).toFraction(false))
                    .addClass(k == stepsCount - 1 ? "res" : ""));
            }

            for (var j = 0; j < m; j++) {

                if (i != n) {
                    row.append($("<td></td>").text(Fraction(step.a[i][j]).toFraction(false))
                        .attr(( i == step.si && j == step.sj) ? {"class": "solvers"} : {}));
                }
                else {
                    row.append($("<td></td>").text(Fraction(step.delta[j]).toFraction(false)));
                }
            }

            row.append($("<td></td>").text(( i != n ) ? Fraction(step.theta[i]).toFraction(false) : ""));

        }

        document.getElementById("result").firstElementChild.lastElementChild.firstElementChild.setAttribute("colspan", "2");
        section = (section == "s1") ? "s2" : "s1";
    }
}

function tableHeader(table, f) {

    var row = $("<tr></tr>"), i;
    table.append(row);

    row.append($("<th></th>").text("Базис"));
    row.append($("<th></th>").text("Сбаз"));
    row.append($("<th></th>").text("План"));

    f.forEach(function (item, i, arr) {
        row.append($("<th></th>").text(item));
    });

    row.append($("<th></th>").text("Theta"));

    row = $("<tr></tr>");
    table.append(row);

    for (i = 1; i <= f.length; i++) {
        row.append($("<th></th>").text("x" + i));
    }

    var firstRow = document.getElementById("result").firstElementChild.firstElementChild;

    for (var elem = firstRow.firstElementChild, i = 0; i < 3; i++) {

        elem.setAttribute("rowspan", "2");
        elem = elem.nextElementSibling;
    }

    firstRow.lastElementChild.setAttribute("rowspan", "2");
}

function strikeCells(arr) {

    for (var i = 0; i < arr.rows.length; i++) {

        if (arr.rows[i] == -1) {

            $(".r" + i).addClass('strike');
        }

    }
    for (var j = 0; j < arr.columns.length; j++) {
        if (arr.columns[j] == -1) {

            $("#matrix input:nth-child(" + (j + 1) + ")").addClass('strike');
        }
    }

}

function normalize(p) {

    var n = p.length,
        m = p[0].length,
        i, j;

    var maxNegative = -Infinity;
    for (i = 0; i < n; i++) {

        for (j = 0; j < m; j++) {

            if (p[i][j] < 0) {
                maxNegative = (p[i][j] > maxNegative) ? p[i][j] : maxNegative;
            }
        }
    }
    var offset = (maxNegative == -Infinity) ? 0 : -maxNegative + 1;

    for (i = 0; i < n; i++) {

        for (j = 0; j < m; j++) {
            p[i][j] += offset;
        }
    }

    setMatrix(p);
    return offset;
}

function setMatrix(p) {

    var n = p.length,
        m = p[0].length,
        i, j;

    for (i = 0; i < n; i++) {

        for (j = 0; j < m; j++) {

            $(".r" + i)[j].value = p[i][j];
        }
    }
}

function showAlphaBeta(obj) {

    var ab = $("#ab")

    var alpha = $("<span></span>").text("Alpha = " + obj.a);
    var beta = $("<span></span>").text("Beta = " + obj.b);

    ab.append(alpha).append("<br>");
    ab.append(beta);
}
